import React, { Component } from 'react';
import Slider, { Range } from 'rc-slider';
import Tooltip from 'rc-tooltip';
import 'rc-slider/assets/index.css';
import './timeSlider.css';

//Create a Custom Handler
const Handle = Slider.Handle;
const handle = (props) => {
  const { value, dragging, index,...restProps } = props;
  var title;
  if(index === 0){
    title = 'Start Time';
  }else if(index === 1){
    title = 'Bed Time';
  }else{
    title = 'End Time';
  }
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={title}
      visible={true}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

//Custom Time Mark Labels
var timeMarks = {
    0: '5 PM',
    1: '6 PM',
    2: '7 PM',
    3: '8 PM',
    4: '9 PM',
    5: '10 PM',
    6: '11 PM',
    7: '12 AM',
    8: {
        style: {
        color: 'red',
        fontWeight: 'bolder'
        },
        label: '1 AM'
    },
    9: {
        style: {
        color: 'red',
        fontWeight: 'bolder'
        },
        label: '2 AM'
    },
    10: {
        style: {
        color: 'red',
        fontWeight: 'bolder'
        },
        label: '3 AM'
    },
    11: {
        style: {
        color: 'red',
        fontWeight: 'bolder'
        },
        label: '4 AM'
    }
};

class timeSlider extends Component {
    constructor(props) {
      super(props);
    }
    render() {
        return (
            <div className='rangeSliderContainer'>
                <Range
                    min={0} 
                    max={11} 
                    marks={timeMarks}
                    count={3}
                    defaultValue={[1, 5, 7]}
                    trackStyle={[
                    {height:10, backgroundColor: 'green'},
                    {height:10, backgroundColor: 'gray'}
                    ]}
                    handleStyle={[
                    {height:28,width:28,marginLeft: -14,marginTop: -9, backgroundColor: 'green'},
                    {height:28,width:28,marginLeft: -14,marginTop: -9, backgroundColor: 'gray'},
                    {height:28,width:28,marginLeft: -14,marginTop: -9, backgroundColor: 'red'},
                    ]}
                    railStyle={{ height:10,backgroundColor: 'black' }}
                    dotStyle={{height:14,width:14,marginTop: -20}}
                    allowCross={false}
                    handle={handle}
                    onChange={this.props.timeChange}
                />
            </div>
        )
    }
}
export default timeSlider;