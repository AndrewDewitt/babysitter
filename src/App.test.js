import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import WageDetails from './wageDetails';
import TimeSlider from './timeSlider';
import each from 'jest-each';
var myApp = new App;

it('App renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('timeSlider renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TimeSlider />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('wageDetails renders without crashing', () => {
  const div = document.createElement('div');
  var hours = {awake:4,asleep:2,overtime:0};
  var wages = {awake:48,asleep:16,overtime:0,gross:64};
  ReactDOM.render(<WageDetails hours={hours} wages={wages}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

//Data for calculating non overtime hours testing
var calculateAwakeAsleepHoursArray = [
  [
    'Normal Awake',
    new Date(new Date().setHours(18,0,0,0)),
    new Date(new Date().setHours(21,0,0,0)),
    3,
  ],
  [
    'Past Midnight Bedtime',
    new Date(new Date().setHours(18,0,0,0)),
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(3,0,0,0)),
    6,
  ],
  [
    'Start Time Past Midnight',
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(1,0,0,0)),
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(4,0,0,0)),
    0,
  ],
  [
    'Normal Asleep',
    new Date(new Date().setHours(21,0,0,0)),
    new Date(new Date().setHours(24,0,0,0)),
    3,
  ],
  [
    'End Time Past Midnight',
    new Date(new Date().setHours(21,0,0,0)),
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(3,0,0,0)),
    3,
  ],
  [
    'Bed Time Past Midnight',
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(1,0,0,0)),
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(4,0,0,0)),
    0,
  ]
];
//Data for calculating overtime hours testing
var calculateOvertimeHoursArray = [
  [
    'Normal End Time',
    new Date(new Date(new Date().setDate(new Date().getDate() + 1)).setHours(3,0,0,0)),
    3,
  ],
  [
    'Before Midnight End Time',
    new Date(new Date().setHours(23,0,0,0)),
    0,
  ]
];
//Data for calculating Wages
var calculateWagesArray = [
  ['Awake and Assleep with No Overtime',4,2,0,{awake:48,asleep:16,overtime:0,gross:64}],
  ['Awake, Asleep, and Overtime',4,2,2,{awake:48,asleep:16,overtime:32,gross:96}],
  ['Awake and Overtime with no asleep',4,0,2,{awake:48,asleep:0,overtime:32,gross:80}],
  ['Asleep and Overtime with no Awake',0,4,2,{awake:0,asleep:32,overtime:32,gross:64}],
  ['Overtime with no Awake or Asleep',0,0,2,{awake:0,asleep:0,overtime:32,gross:32}],
  ['No hours',0,0,0,{awake:0,asleep:0,overtime:0,gross:0}]
];
//Data for Time Change
var timeChangeArray = [
  ['No Overtime',[0,5,7],{hours:{awake:5,asleep:2,overtime:0},wages:{awake:60,asleep:16,overtime:0,gross:76}}],
  ['With Overtime',[0,5,9],{hours:{awake:5,asleep:2,overtime:2},wages:{awake:60,asleep:16,overtime:32,gross:108}}],
  ['No Sleep',[0,5,5],{hours:{awake:5,asleep:0,overtime:0},wages:{awake:60,asleep:0,overtime:0,gross:60}}],
  ['No Awake',[5,5,7],{hours:{awake:0,asleep:2,overtime:0},wages:{awake:0,asleep:16,overtime:0,gross:16}}],
  ['Full Time No Sleep',[0,11,11],{hours:{awake:7,asleep:0,overtime:4},wages:{awake:84,asleep:0,overtime:64,gross:148}}],
  ['Full Time Midnight Bedtime',[0,7,11],{hours:{awake:7,asleep:0,overtime:4},wages:{awake:84,asleep:0,overtime:64,gross:148}}],
  ['No Time',[0,0,0],{hours:{awake:0,asleep:0,overtime:0},wages:{awake:0,asleep:0,overtime:0,gross:0}}]
];

//Awake Asleep Hours Test
each(calculateAwakeAsleepHoursArray).test('%s Hours', (name, start, end, expected) => {
  var calculatedAwakeHours = myApp.calculateAwakeAsleepHours(start,end);
  expect(calculatedAwakeHours).toEqual(expected);
});

//Awake Overtime Hours Test
each(calculateOvertimeHoursArray).test('%s Overtime Hours', (name, end, expected) => {
  var calculatedAwakeHours = myApp.calculateOvertimeHours(end);
  expect(calculatedAwakeHours).toEqual(expected);
});

//Calculate Wages Test
each(calculateWagesArray).test('Calculate Wage %s', (name, awakeHours, asleepHours, overtimeHours, expected) => {
  var calculatedWages = myApp.calculateWage(awakeHours, asleepHours, overtimeHours);
  expect(calculatedWages).toEqual(expected);
});

//Time Change Test
each(timeChangeArray).test('Time Change %s', (name, value, expected) => {
  //Need to render this to prevent warning since this method sets state
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  var timeChanged = myApp.timeChange(value);
  expect(timeChanged).toEqual(expected);
  ReactDOM.unmountComponentAtNode(div);
});